# AWS-Infra-Orchestrator-With-Validator

This repository provides a framework to provision AWS cloud resources with automated assessment for security, compliance and user-defined standards. 

## Getting started

1. Fork/Clone this repo

2. Onboard Cloud Account
Add your cloud crendentials as CI/CD variables in the project settings.

3. Templates

Folder Structure:
```
.
├── README.md
├── assess.sh
├── recipes
│   ├── ec2.rb
│   └── sg.rb
├── templates
│   ├── CreateS3Bucket
│   └── EC2InstanceWithSecurityGroup
```

There are few sample CFN templates in the `templates` folder to get started. Any user created templates should go here.

4. Chef Inspec [Recipes]

Further, the assessment is done through `chef inspec`. The recipes for sample assessment is located in `recipes` folder. Please use this as reference to further customize/extend the assessment. Appropriate conditions should also be added to the `assess.sh` script.

5. Allure Dashboard

Assessment reports are sent to Allure service, and available on a elegant UI [Dashboard](https://allure-karthick.koyeb.app/allure-docker-service-ui/projects/aws-inspec).

PS: If you intend to use this for production, self-host a dedicated allure service+UI and enable security as documented here - https://github.com/fescobar/allure-docker-service#enable-security

## Test and Deploy

- [ ] Commit a new template(s) or update parameters.json for one or more templates
- [ ] Gitlab CI/CD will perform
    - create/update a new CFN stack for each template
    - perform infrastructure assessment using chef inspec
    - send results to allure ui

## FAQ

1. How this is different from AWS Inspector?
- Unlike AWS Inspector which is performed as an on-demand assessment, this framework is triggered only at the time of resource provisioning
- Pricing - No limits on the number of assessment, whereas AWS Inspector has its own charging  policy once the assessment count exceed 250
- Extensible, customizable, scope for multi-cloud support
- Free and Open Source Implementation

2. Can't access allure UI/Dashboard?
- OpenDNS and DNS blocking mechanism is known to block koyeb.app DN. Exclude the domain/switch to alternate DNS servers.

## Roadmap
- migrate CFN to terraform to support multi-cloud
- extend assessment with more recipes


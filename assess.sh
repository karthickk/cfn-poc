#!/bin/bash
#
stack=$1
aws cloudformation list-stack-resources     --stack-name $stack > /tmp/stack.json
jq -r '.StackResourceSummaries[]| "\(.LogicalResourceId) \(.PhysicalResourceId)"' /tmp/stack.json > /tmp/resources

rm -rf validator
mkdir -p validator/controls
echo "name: $stack" > validator/inspec.yml
echo "version: 1.0" >> validator/inspec.yml

cat /tmp/resources | while read line
do
	type=`echo $line|awk '{print $1}'`
	id=`echo $line|awk '{print $2}'`
	if [[ "$type" == "EC2Instance" ]]
	then
		sed "s/instanceid/$id/g" recipes/ec2.rb > validator/controls/ec2.rb
	fi
	if [[ "$type" == "InstanceSecurityGroup" ]]
	then
			sed "s/sgname/$id/g" recipes/sg.rb > validator/controls/sg.rb
	fi
	if [[ "$type" == "S3Bucket" ]]
	then
			sed "s/bucketname/$id/g" recipes/s3.rb > validator/controls/s3.rb
	fi
done
if [ -d validator ]
then
PATH=$(/usr/bin/printenv PATH | /usr/bin/perl -ne 'print join(":", grep { !/\/mnt\/[a-z]/ } split(/:/));') inspec exec -t aws:// validator --chef-license accept-silent --reporter cli junit2:/tmp/junit.xml --silence-deprecations=all
fi

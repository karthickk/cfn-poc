control "s3" do
title "Testing s3"
describe "s3 validation"
describe aws_s3_bucket('bucketname') do
  it { should have_versioning_enabled }
end
describe aws_s3_bucket('bucketname') do
  its('bucket_acl.count') { should be >= 1 }
end
end

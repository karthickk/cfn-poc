control "sg" do
title "Testing sg"
describe "sg validation"
describe aws_security_group(group_name: 'sgname') do
    it { should exist }
end
describe aws_security_group(group_name: 'sgname') do
    it { should_not allow_in(port: 22, ipv4_range: '0.0.0.0/0') }
    it { should_not allow_in(port: 80) }
end
end

control "ec2" do
title "Testing ec2"
describe "Some additional description"
describe aws_ec2_instance('instanceid') do
    it { should exist }
end
describe aws_ec2_instance('instanceid') do
    its('tags') { should include(key: 'env', value: 'test') }
end
describe aws_ec2_instance('instanceid') do
  it { should_not have_roles }
end
describe aws_ec2_instance('instanceid') do
  its('instance_type') { should be_in 't2.micro,t2.nano' }
end
end
